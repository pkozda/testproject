Test project for SPA of education list.


## Installation

In order to run this site you have to make the following steps:


1. Move to folder where you want this project to be.

2. Clone this repository to your pc.
    ```
    git clone https://pkozda@bitbucket.org/pkozda/testproject.git
    ```

3. Move to created folder testproject.
    ```
    cd testproject
    ```

4. Create and activate a new virtual environment.
    ```
    virtualenv .venv
    ```
    and then
    ```
    source .venv/bin/activate
    ```

5. Install python dependencies with **pip**.
    ```
    pip install -r requirements.txt
    ```

6. Run database migrations.
    ```
    python manage.py migrate
    ```

7. Load fixtures.
    ```
    python manage.py loaddata fixtures.json
    ```

8. Start local server.
    ```
    python manage.py runserver
    ```

9. Now you can navigate to http://localhost:8000/#/education in your browser.