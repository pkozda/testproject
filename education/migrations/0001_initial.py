# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Education',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=100)),
                ('entrance_date', models.DateField()),
                ('graduate_date', models.DateField()),
                ('graduate', models.CharField(max_length=1, choices=[(b'm', b'Master'), (b'e', b'Expert'), (b'b', b'Bachelor')])),
            ],
        ),
    ]
