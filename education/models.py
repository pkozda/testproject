from django.db import models


class Education(models.Model):
    """Model for creating education data"""

    GRADUATES = (
        ('m', 'Master'),
        ('e', 'Expert'),
        ('b', 'Bachelor'),
    )
    name = models.CharField(max_length=100)
    entrance_date = models.DateField()
    graduate_date = models.DateField()
    graduate = models.CharField(max_length=1, choices=GRADUATES)

    def __str__(self):
        return self.name
