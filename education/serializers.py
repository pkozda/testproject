from rest_framework import serializers
from education.models import Education


class EducationSerializer(serializers.HyperlinkedModelSerializer):
    graduate_name = serializers.SerializerMethodField()

    class Meta:
        model = Education
        fields = ('id', 'name', 'entrance_date', 'graduate_date',
                  'graduate', 'graduate_name')

    def get_graduate_name(self, obj):
        return obj.get_graduate_display()
