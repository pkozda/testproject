from rest_framework import viewsets
from education.serializers import EducationSerializer
from education.models import Education
from django.shortcuts import render


def index(request):
    return render(request, 'education/index.html')


class EducationViewSet(viewsets.ModelViewSet):
    """
    This endpoint presents education list.
    """
    queryset = Education.objects.all()
    serializer_class = EducationSerializer
