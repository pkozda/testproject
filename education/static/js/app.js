'use strict';

var app = angular.module('app',
    ['ui.router', 'ngResource', 'ngCookies', 'ui.bootstrap']);

app.run( function run( $http, $cookies ){
    // For CSRF token compatibility with Django
    $http.defaults.headers.post['X-CSRFToken'] = $cookies['csrftoken'];
});

/* Router */
app.config (['$stateProvider', '$urlRouterProvider',
    function ($stateProvider, $urlRouterProvider) {

    $urlRouterProvider.otherwise('/education');

    $stateProvider
        .state('education', {
            url: '/education',
            templateUrl: 'static/templates/education.html',
            controller: 'MainCtrl'
        })
        .state('education.detail', {
            url: '/:id',
            templateUrl: 'static/templates/detail.html',
            controller: 'DetailCtrl'
        })
        .state('add', {
            url: '/add',
            templateUrl: 'static/templates/add.html',
            controller: 'AddCtrl'
        })
}]);