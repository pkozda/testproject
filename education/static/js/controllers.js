'use strict';

/* Controllers */
// Main controller
app.controller ('MainCtrl', ['$scope', '$http', '$state', 'EducationList',
    function ($scope, $http, $state, EducationList) {

        function load(){
            $scope.educationList = EducationList.query();
        }
        load();

        $scope.$on('updated', load);

        $scope.removeEducation = function () {
            var id = this.education.id;
            var my_url = '/api/education/' + id;
            $http.delete(my_url, {params: {id: id}});
            $scope.$emit('updated');
            $state.go('education');
        };
    }])

// Detail controller
    .controller ('DetailCtrl', ['$scope', '$location', '$stateParams', 'EducationList', '$state',
    function ($scope, $location, $stateParams, EducationList, $state) {

        $scope.education = EducationList.get({id: $stateParams.id});

        $scope.editEducation = function(){
            EducationList.update($scope.education, function(){
                $scope.$emit('updated');
                $state.go('education');
            });
        };

        $scope.closeEditor = function () {
            $state.go('education');
        };
    }])

    .controller('AddCtrl', ['$scope', 'EducationList', '$state',
        function($scope, EducationList, $state) {

        $scope.education = {
            "name": "",
            "graduate": "",
            "entrance_date": '',
            "graduate_date": ''
        };

        $scope.addEducation = function() {
            EducationList.create($scope.education);
            $state.go('education');
        };

        $scope.closeEditor = function () {
            $state.go('education');
        };
    }])

// Datepicker controller
    .controller('DatepickerCtrl', function ($scope) {

        $scope.open = function($event) {
            $scope.status.opened = true;
        };

        $scope.format = 'dd.MM.yyyy';

        $scope.status = {
            opened: false
        };

    });