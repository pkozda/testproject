'use strict';

app.factory ('EducationList', ['$resource', function ($resource) {

    return $resource('/api/education/:id', {id: '@id'}, {
        query: {
            isArray: true,
            method: 'GET',
            params: {},
            transformResponse: function (data) {
            return JSON.parse(data).results;
        }
    },
        create: {
            method: 'POST',
            transformRequest: dateTransformer
        },
        update: {
            method: 'put',
            isArray: false,
            transformRequest: dateTransformer
        }
    });
}]);


function dateTransformer(data, headersGetter) {
    console.log('Request trasformer is called', data, headersGetter);

        for (var field in data) {
            if (Object.hasOwnProperty.call(data, field)) {
                if (angular.isDate(data[field])) {
                    console.log(field, 'contains a Data object.');
                    data[field] = getFormatDate(data[field]);
                }
            }
        }

        return angular.toJson(data);
    }

function getFormatDate(dateFrom){
      dateFrom = dateFrom.getFullYear() + '-' + ('0' + (dateFrom.getMonth() + 1)).slice(-2) + '-' + ('0' + dateFrom.getDate()).slice(-2);
      return dateFrom;
    }