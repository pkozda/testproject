from django.contrib import admin
from education.models import Education


class EducationAdmin(admin.ModelAdmin):
    list_display = ('name', 'entrance_date', 'graduate_date', 'graduate')
    list_filter = ('entrance_date', 'graduate_date')
    search_fields = ('name', 'graduate')


admin.site.register(Education, EducationAdmin)
